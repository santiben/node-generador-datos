const sequelize = require('./src/models');

const argv = require('./src/yargs').argv;

const {
    showTables,
    showTable,
    insertDataAllTables,
    insertDataToTable,
    deleteDataAllTable,
    deleteDataToTable,
} = require('./src/controllers/sql-command');

sequelize.sync().then((data) => {
    console.log('Listen database postgres');

    console.log(argv);

    const command = argv._[0];
    let option;
    switch (command) {
        case 'show':
            option = argv._[1];
            switch (option) {
                case 'tables':
                    showTables(argv.list, argv.content);
                    break;
                case 'table':
                    showTable(argv.name);
                    break;
            }
            break;
        case 'insert':
            option = argv._[1];
            switch (option) {
                case 'tables':
                    insertDataAllTables(data.options.dialect);
                    break;
                case 'table':
                    insertDataToTable(argv.name, argv.count, data.options.dialect);
                    break;
            }
            break;
        case 'delete':
            option = argv._[1];
            switch (option) {
                case 'tables':
                    deleteDataAllTable();
                    break;
                case 'table':
                    deleteDataToTable(argv.name);
                    break;
            }
            break;
    }

}).catch(error => {
    console.log('Error connection database', error);
});