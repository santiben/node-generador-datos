CREATE DATABASE bd_multidimensional;

CREATE TABLE estados(
  idEstado VARCHAR(5) NOT NULL,
  nombre VARCHAR(30) NOT NULL,
  CONSTRAINT pk_estados_idEstado PRIMARY KEY (idEstado)
);

CREATE TABLE ciudades(
  idCiudad VARCHAR(5) NOT NULL,
  nombre VARCHAR(30) NOT NULL,
  poblacion INTEGER NOT NULL,
  idEstado VARCHAR(5) NOT NULL,
  CONSTRAINT pk_ciudades_idCiudad PRIMARY KEY (idCiudad),
  CONSTRAINT fk_ciudades_estados_idEstado FOREIGN KEY (idEstado) REFERENCES estados(idEstado)
);

CREATE TABLE hospitales(
  idHosp VARCHAR(5) NOT NULL,
  nombre VARCHAR(30) NOT NULL,
  tipo VARCHAR(20) NOT NULL,
  idCiudad VARCHAR(5) NOT NULL,
  CONSTRAINT pk_hospitales_idHosp PRIMARY KEY (idHosp),
  CONSTRAINT fk_hospitales_ciudades_idCiudad FOREIGN KEY (idCiudad) REFERENCES ciudades(idCiudad)
);

CREATE TABLE doctores(
  idDoctor VARCHAR(10) NOT NULL,
  nombre VARCHAR(30) NOT NULL,
  apellidopaterno VARCHAR(30) NOT NULL,
  apellidomaterno VARCHAR(30) NOT NULL,
  sueldo INTEGER NOT NULL,
  especialidad VARCHAR(30) NOT NULL,
  idHosp VARCHAR(5) NOT NULL,
  CONSTRAINT pk_doctores_idDoctor PRIMARY KEY (idDoctor),
  CONSTRAINT fk_doctores_hospitales_idHosp FOREIGN KEY (idHosp) REFERENCES hospitales(idHosp)
);

CREATE TABLE desarrollofisico(
  idEtapaVida VARCHAR(5) NOT NULL,
  nombre VARCHAR(30) NOT NULL,
  edadLimInf INTEGER NOT NULL,
  edadLimSup INTEGER NOT NULL,
  CONSTRAINT pk_desarrollofisico_idEtapaVida PRIMARY KEY (idEtapaVida)
);

CREATE TABLE pacientes(
  idPaciente VARCHAR(10) NOT NULL,
  nombre VARCHAR(30) NOT NULL,
  apellidopaterno VARCHAR(30) NOT NULL,
  apellidomaterno VARCHAR(30) NOT NULL,
  edad INTEGER NOT NULL,
  sexo VARCHAR(1) NOT NULL,
  idCiudad VARCHAR(5) NOT NULL,
  idEtapaVida VARCHAR(5) NOT NULL,
  CONSTRAINT pk_pacientes_idPaciente PRIMARY KEY (idPaciente),
  CONSTRAINT fk_pacientes_desarrollofisico_idEtapaVida FOREIGN KEY (idEtapaVida) REFERENCES desarrollofisico(idEtapaVida)
);

CREATE TABLE facturas(
  idFactura VARCHAR(5) NOT NULL,
  fecha DATE,
  hora TIME,
  idHosp VARCHAR(5) NOT NULL,
  idPaciente VARCHAR(10) NOT NULL,
  subtotal NUMERIC(6, 2) NOT NULL,
  iva NUMERIC(6, 2) NOT NULL,
  total NUMERIC(6, 2) NOT NULL,
  CONSTRAINT pk_facturas_idFactura PRIMARY KEY (idFactura),
  CONSTRAINT fk_facturas_hospitales_idHosp FOREIGN KEY (idHosp) REFERENCES hospitales(idHosp),
  CONSTRAINT fk_facturas_pacientes_idPaciente FOREIGN KEY (idPaciente) REFERENCES pacientes(idPaciente)
);

CREATE TABLE conceptos(
  idConcepto VARCHAR(5) NOT NULL,
  nombre VARCHAR(30) NOT NULL,
  CONSTRAINT pk_conceptos_idConcepto PRIMARY KEY (idConcepto)
);

CREATE TABLE factdetalle(
  idFactura VARCHAR(5) NOT NULL,
  idConcepto VARCHAR(5) NOT NULL,
  cant INTEGER NOT NULL,
  precioUnit INTEGER NOT NULL,
  subtotalConcept INTEGER NOT NULL,
  iva NUMERIC(6, 2) NOT NULL,
  totalConc NUMERIC(6, 2) NOT NULL,
  idDoctor VARCHAR(10) NOT NULL,
  CONSTRAINT pk_factdetalle_idFactura_idConcepto PRIMARY KEY (idFactura, idConcepto),
  CONSTRAINT fk_factdetalle_facturas_idFactura FOREIGN KEY (idFactura) REFERENCES facturas(idFactura),
  CONSTRAINT fk_factdetalle_conceptos_idConcepto FOREIGN KEY (idConcepto) REFERENCES conceptos(idConcepto),
  CONSTRAINT fk_factdetalle_doctores_idDoctor FOREIGN KEY (idDoctor) REFERENCES doctores(idDoctor)
);
