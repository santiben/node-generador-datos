let argv = require('yargs')
    .command('show', 'Mostrar datos de la base de datos', function (yargs) {
        yargs
            .command('tables', 'Mostrar tablas de la base de datos', {
                list: {
                    demand: false,
                    alias: 'l',
                    desc: 'Enlistar los nombres de las tablas de la base de datos',
                },
                content: {
                    demand: false,
                    alias: 'c',
                    desc: 'Mostrar el contenido de las tablas de la base de datos',
                }
            })
            .command('table', 'Mostrar los datos de una tabla de la base de datos', {
                name: {
                    demand: true,
                    alias: 'n',
                    desc: 'Nombre de la tabla de la base de datos',
                }
            })
            .choices('name', ['ciudades', 'conceptos', 'desarrollofisico', 'doctores', 'estados', 'factdetalle', 'facturas', 'hospitales', 'pacientes'])
    })
    .command('insert', 'Insertar datos en la tablas de la base de datos', function (yargs) {
        yargs
            .command('tables', 'Insertar los datos en la tabla de la base de datos')
            .command('table', 'Insertar los datos en la tabla de la base de datos', {
                name: {
                    demand: true,
                    alias: 'n',
                    desc: 'Nombre de la tabla de la base de datos',
                },
                count: {
                    alias: 'c',
                    default: 10,
                    desc: 'Número de registros'
                }
            })
            .choices('name', ['ciudades', 'conceptos', 'desarrollofisico', 'doctores', 'estados', 'factdetalle', 'facturas', 'hospitales', 'pacientes'])
    })
    .command('delete', 'Eliminar datps de las tablas de la base de datos', function (yargs) {
        yargs
            .command('tables', 'Eliminar los datos de las tablas de la base de datos')
            .command('table', 'Eliminar los datos en la tabla de la base de datos', {
                name: {
                    demand: true,
                    alias: 'n',
                    desc: 'Nombre de la tabla de la base de datos',
                }
            })
            .choices('name', ['ciudades', 'conceptos', 'desarrollofisico', 'doctores', 'estados', 'factdetalle', 'facturas', 'hospitales', 'pacientes'])
    })
    .help()
    .argv;

module.exports = {
    argv
};