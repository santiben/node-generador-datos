/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('facturas', {
        idfactura: {
            type: DataTypes.STRING,
            allowNull: false,
            primaryKey: true
        },
        fecha: {
            type: DataTypes.DATEONLY,
            allowNull: true
        },
        hora: {
            type: DataTypes.TIME,
            allowNull: true
        },
        idhosp: {
            type: DataTypes.STRING,
            allowNull: false,
            references: {
                model: 'hospitales',
                key: 'idhosp'
            }
        },
        idpaciente: {
            type: DataTypes.STRING,
            allowNull: false,
            references: {
                model: 'pacientes',
                key: 'idpaciente'
            }
        },
        subtotal: {
            type: DataTypes.DOUBLE,
            allowNull: false
        },
        iva: {
            type: DataTypes.DOUBLE,
            allowNull: false
        },
        total: {
            type: DataTypes.DOUBLE,
            allowNull: false
        }
    }, {
        createdAt: false,
        updatedAt: false,
        tableName: 'facturas'
    });
};
