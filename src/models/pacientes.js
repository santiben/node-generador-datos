/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('pacientes', {
        idpaciente: {
            type: DataTypes.STRING,
            allowNull: false,
            primaryKey: true
        },
        nombre: {
            type: DataTypes.STRING,
            allowNull: false
        },
        apellidopaterno: {
            type: DataTypes.STRING,
            allowNull: false
        },
        apellidomaterno: {
            type: DataTypes.STRING,
            allowNull: false
        },
        edad: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        sexo: {
            type: DataTypes.STRING,
            allowNull: false
        },
        idciudad: {
            type: DataTypes.STRING,
            allowNull: false
        },
        idetapavida: {
            type: DataTypes.STRING,
            allowNull: false,
            references: {
                model: 'desarrollofisico',
                key: 'idetapavida'
            }
        }
    }, {
        createdAt: false,
        updatedAt: false,
        tableName: 'pacientes'
    });
};
