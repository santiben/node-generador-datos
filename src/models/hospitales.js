/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('hospitales', {
        idhosp: {
            type: DataTypes.STRING,
            allowNull: false,
            primaryKey: true
        },
        nombre: {
            type: DataTypes.STRING,
            allowNull: false
        },
        tipo: {
            type: DataTypes.STRING,
            allowNull: false
        },
        idciudad: {
            type: DataTypes.STRING,
            allowNull: false,
            references: {
                model: 'ciudades',
                key: 'idciudad'
            }
        }
    }, {
        createdAt: false,
        updatedAt: false,
        tableName: 'hospitales'
    });
};
