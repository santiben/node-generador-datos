
module.exports = function (sequelize, DataTypes) {
    return sequelize.define('desarrollofisico', {
        idetapavida: {
            type: DataTypes.STRING,
            allowNull: false,
            primaryKey: true
        },
        nombre: {
            type: DataTypes.STRING,
            allowNull: false
        },
        edadliminf: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        edadlimsup: {
            type: DataTypes.INTEGER,
            allowNull: false
        }
    }, {
        createdAt: false,
        updatedAt: false,
        tableName: 'desarrollofisico'
    });
};
