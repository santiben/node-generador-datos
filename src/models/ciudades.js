/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('ciudades', {
        idciudad: {
            type: DataTypes.STRING,
            allowNull: false,
            primaryKey: true
        },
        nombre: {
            type: DataTypes.STRING,
            allowNull: false
        },
        poblacion: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        idestado: {
            type: DataTypes.STRING,
            allowNull: false,
            references: {
                model: 'estados',
                key: 'idestado'
            }
        }
    }, {
        createdAt: false,
        updatedAt: false,
        tableName: 'ciudades'
    });
};
