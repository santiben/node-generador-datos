const path = require('path');
require('dotenv/config');
const Sequelize = require('sequelize');

/*const sequelize = new Sequelize(process.env.DB, process.env.USER, process.env.PASSWORD, {
    dialect: process.env.DIALECT,
    host: process.env.HOST,
    operatorsAliases: false,
    logging: false,

    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
});*/

const sequelize = new Sequelize('bd_multidimensional', '', '', {
    host: '',
    dialect: '',
    operatorsAliases: false,
    logging: false,

    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
});

sequelize.import(path.join(__dirname, 'ciudades'));
sequelize.import(path.join(__dirname, 'conceptos'));
sequelize.import(path.join(__dirname, 'desarrollofisico'));
sequelize.import(path.join(__dirname, 'doctores'));
sequelize.import(path.join(__dirname, 'estados'));
sequelize.import(path.join(__dirname, 'factdetalle'));
sequelize.import(path.join(__dirname, 'facturas'));
sequelize.import(path.join(__dirname, 'hospitales'));
sequelize.import(path.join(__dirname, 'pacientes'));

module.exports = sequelize;