/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('doctores', {
        iddoctor: {
            type: DataTypes.STRING,
            allowNull: false,
            primaryKey: true
        },
        nombre: {
            type: DataTypes.STRING,
            allowNull: false
        },
        apellidopaterno: {
            type: DataTypes.STRING,
            allowNull: false
        },
        apellidomaterno: {
            type: DataTypes.STRING,
            allowNull: false
        },
        sueldo: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        especialidad: {
            type: DataTypes.STRING,
            allowNull: false
        },
        idhosp: {
            type: DataTypes.STRING,
            allowNull: false,
            references: {
                model: 'hospitales',
                key: 'idhosp'
            }
        }
    }, {
        createdAt: false,
        updatedAt: false,
        tableName: 'doctores'
    });
};
