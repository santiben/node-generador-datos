/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('factdetalle', {
        idfactura: {
            type: DataTypes.STRING,
            allowNull: false,
            primaryKey: true,
            references: {
                model: 'facturas',
                key: 'idfactura'
            }
        },
        idconcepto: {
            type: DataTypes.STRING,
            allowNull: false,
            references: {
                model: 'conceptos',
                key: 'idconcepto'
            }
        },
        cant: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        preciounit: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        subtotalconcept: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        iva: {
            type: DataTypes.DOUBLE,
            allowNull: false
        },
        totalconc: {
            type: DataTypes.DOUBLE,
            allowNull: false
        },
        iddoctor: {
            type: DataTypes.STRING,
            allowNull: false,
            references: {
                model: 'doctores',
                key: 'iddoctor'
            }
        }
    }, {
        createdAt: false,
        updatedAt: false,
        tableName: 'factdetalle'
    });
};
