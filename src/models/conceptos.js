/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('conceptos', {
        idconcepto: {
            type: DataTypes.STRING,
            allowNull: false,
            primaryKey: true
        },
        nombre: {
            type: DataTypes.STRING,
            allowNull: false
        }
    }, {
        createdAt: false,
        updatedAt: false,
        tableName: 'conceptos'
    });
};
