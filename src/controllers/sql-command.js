const sequelize = require('../models');
const { models } = sequelize;
const _ = require('underscore');
const fs = require('fs');

module.exports = {

    showTables: async (list, content) => {

        try {

            if (list) {
                const sql = await sequelize.query('SELECT table_name FROM information_schema.tables WHERE table_schema = \'public\';',
                    {type: sequelize.QueryTypes.SELECT});
                console.log(sql);
            } else if (content) {
                const tables = ['estados', 'ciudades', 'hospitales', 'doctores', 'desarrollofisico', 'pacientes', 'facturas', 'conceptos', 'factdetalle'];

                for (let x = 0; x < tables.length; x++) {
                    const result =  await eval(`sequelize.query('SELECT * FROM ${tables[x]}', {type: sequelize.QueryTypes.SELECT})`);
                    console.log(result);
                }
            }

        } catch (e) {
            console.error(e);
        }

    },

    showTable: async (table) => {

        try {

            const sql = await sequelize.query(`SELECT * FROM ${table}`,
                {type: sequelize.QueryTypes.SELECT});
            console.log(sql);

        } catch (e) {
            console.error(e);
        }

    },

    insertDataAllTables: async (dialect) => {
        try {
            const file = fs.readFileSync('data.json', 'utf-8');
            const jsonFile = JSON.parse(file);

            const tables = ['estados', 'ciudades', 'hospitales', 'doctores', 'desarrollofisico', 'pacientes', 'facturas', 'conceptos', 'factdetalle'];

            switch (dialect) {
                case 'postgres':
                    for (let x = 0; x < tables.length; x++) {
                        for (let i = 0; i < jsonFile.data.length; i++) {
                            if (tables[x] === jsonFile.data[i].table) {
                                const result =  await eval(`models.${tables[x]}.bulkCreate(jsonFile.data[i].item)`);
                                console.log(result);
                            }
                        }
                    }
                    break;
                case 'mysql':
                    for (let x = 0; x < tables.length; x++) {
                        for (let i = 0; i < jsonFile.data.length; i++) {
                            if (tables[x] === jsonFile.data[i].table) {
                                const result =  await eval(`models.${tables[x]}.bulkCreate(jsonFile.data[i].item)`);
                                console.log(result);
                            }
                        }
                    }
                    break;
                case 'mssql':
                    for (let x = 0; x < tables.length; x++) {
                        for (let i = 0; i < jsonFile.data.length; i++) {
                            if (tables[x] === jsonFile.data[i].table) {
                                _.each(jsonFile.data[i].item, function (item, itemIndex) {
                                    let keys = _.keys(item);
                                    _.each(_.values(item), function (val, valIndex) {
                                        if (!isNaN(val)) {
                                            jsonFile.data[i].item[itemIndex][keys[valIndex]] = parseFloat(val);
                                        }
                                    });
                                });
                                const result =  await eval(`models.${tables[x]}.bulkCreate(jsonFile.data[i].item)`);
                                console.log(result);
                            }
                        }
                    }
                    break;
            }

        } catch (e) {
            console.error(e);
        }
    },

    insertDataToTable: async (table, count, dialect) => {

        try {

            const result = await sequelize.query(`SELECT COUNT(*) FROM ${table}` ,
                {
                    type: sequelize.QueryTypes.SELECT
                });

            const countRow = (dialect === 'mssql' || dialect === 'mysql' ? _.values(result[0])[0] : result[0].count);

            if (countRow > 0) {
                //Table full
                const data = await eval(`models.${table}.findAll()`);
                const dataJson = JSON.parse(JSON.stringify(data));
                const propTable = _.keys(dataJson[0]);
                let arrayObjects = [];
                let maxId = await sequelize.query(`SELECT MAX(${propTable[0]}) FROM ${table}`,
                    {
                        type: sequelize.QueryTypes.SELECT
                    });
                maxId = (dialect === 'mssql' || dialect === 'mysql' ? _.values(maxId[0])[0] : maxId[0].max);

                const idSplit = maxId.split('-');
                let idLetter = idSplit[0];
                let idNumeric = parseInt(idSplit[1]);
                for (let x = 0; x < count; x++) {
                    let item = {};
                    idNumeric += 1;
                    for (let i = 0; i < propTable.length; i++) {
                        if (i === 0) {
                            item[propTable[i]] = `${idLetter}-${(idNumeric > 99 ? idNumeric : '0'+idNumeric)}`;
                        } else {
                            const random = Math.floor(Math.random() * (dataJson.length - 0)) + 0;
                            item[propTable[i]] = _.property(propTable[i])(dataJson[random]);
                        }
                    }
                    arrayObjects.push(item);
                }
                const result =  await eval(`models.${table}.bulkCreate(arrayObjects)`);
                console.log(result);
            } else {
                //Table empty
                const file = fs.readFileSync('data.json', 'utf-8');
                const jsonFile = JSON.parse(file);

                let itemTable = [];
                _.each(jsonFile.data, function (val) {
                    if (val.table === table) {
                        itemTable = val.item;
                    }
                });

                if (itemTable.length > 0) {
                    const result =  await eval(`models.${table}.bulkCreate(itemTable)`);
                    console.log(result);
                } else {
                    console.error(`La tabla ${table} esta vacia en el archivo data.json`);
                }
            }

        } catch (e) {
            console.error(e);
        }

    },

    deleteDataAllTable: async () => {

        try {

            const tables = ['factdetalle', 'conceptos', 'facturas', 'pacientes', 'desarrollofisico', 'doctores', 'hospitales', 'ciudades', 'estados'];
            _.each(tables, async function (val) {
                const result = await sequelize.query(`DELETE FROM ${val}`, {type: sequelize.QueryTypes.DELETE});
                console.log(result);
            });

        } catch (e) {
            console.error(e);
        }

    },

    deleteDataToTable: async (table) => {

        try {

            const result = await sequelize.query(`DELETE FROM ${table}`, {type: sequelize.QueryTypes.DELETE});
            console.log(result);

        } catch (e) {
            console.error(e);
        }

    }

};